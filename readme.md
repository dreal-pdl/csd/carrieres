# Présentation

L’application **« Carrières en Pays de la Loire »**  permet d'afficher les données sur le suivi des carrières en Pays de la Loire, ainsi que 
des indicateurs et des cartes avec des analyses thématiques.

Le volet cartographique vient en complément :
* du projet QGIS disponible sur le réseau interne de la DREAL : S:\CARTES_PAC\RESSOURCES_MINERALES\r_ressource_minerale_r52.qgs
* des cartes interactives mises à disposition du grand sur la plate-forme SIGLOIRE : https://carto.sigloire.fr/1/layers/n_icpe_carriere_p_r52.map

L’application est accessible via l’Intranet de la DREAL et propose les rubriques suivantes :

## Page d'accueil

- Carte des carrières en fonctionnement dans la région Pays de la Loire 
- Sélection d’indicateurs annuels

## Liste des carrières

Affichage de la liste des carrières de la région Pays de la Loire  avec les fonctionnalités suivantes :

- recherche plein texte,
- tri par colonne,
- filtre par colonne,
- export des données par :
   - copie dans le presse-papiers,
   - impression,
   - téléchargement d'un fichier au format .xlsx. 


## Statistiques

- Graphiques et tableaux du nombre de carrières :
   - par département,
   - par type de carrière,
   - par type de ressource
- Évolution de la production :
   - par année
   - par département
   - par type de carrière
   - par type de ressource
   
## Cartes

- Analyses thématiques :
   - par statut de la carrière,
   - par type de carrière,
   - par type de ressource,
   - par volume de production

## Aide

- Présentation de l’application
- Glossaire

## Développement

- Journal des modifications
- Feuille de route

## Mentions légales

## Nous contacter

Lien vers la boîte aux lettres fonctionnelle ADL.

----------

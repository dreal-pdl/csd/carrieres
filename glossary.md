# Glossaire

## Roches massives

Carrières produisant des granulats pour le BTP à partir de roches massives.

## Roches meubles

Carrières produisant des granulats pour le BTP à partir de roches meubles à savoir :
* Sables et graviers alluvionnaires en lit majeur
* Sables et graviers alluvionnaires hors lit majeur
* Autres sables et graviers

## Autres matériaux

* Calcaire à autres usage que granulats pour BTP (calcaire cimentier, à usage agricole ou autre usage industriel)
* Sables et graviers à usage industriel
* Argiles
* Marnes
* Faluns
* Tuffeau
* Cendres volcaniques
* Tous matériaux à usage "roches ornementales et de construction" (schistes, ardoises…)

Les carrières produisant plusieurs types de matériaux sont référencées "Roches massives/Autres matériaux" ou "Roches meubles/Autres matériaux".